import firebase from 'firebase'
import { firebaseConfig, fcmKey } from '~/config_firebase.js'

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();
messaging.usePublicVapidKey(fcmKey);

function resetUI() {
  // console.log('loading...');
  // Get Instance ID token. Initially this makes a network call, once retrieved
  // subsequent calls to getToken will return from cache.
  messaging.getToken().then((currentToken) => {
    if (currentToken) {
      sendTokenToServer(currentToken);
      console.log(currentToken);
    } else {
      // Show permission request.
      console.log('No Instance ID token available. Request permission to generate one.');
      // Show permission UI.
      // updateUIForPushPermissionRequired();
      setTokenSentToServer(false);
    }
  }).catch((err) => {
    console.log('An error occurred while retrieving token. ', err);
    console.log('Error retrieving Instance ID token. ', err);
    setTokenSentToServer(false);
  });
  // [END get_token]
}


function setTokenSentToServer(sent) {
  window.localStorage.setItem('sentToServer', sent ? '1' : '0');
}

function isTokenSentToServer() {
  return window.localStorage.getItem('sentToServer') === '1';
}

function sendTokenToServer(currentToken) {
  if (!isTokenSentToServer()) {
    // console.log('Sending token to server...');
    $axios.post('/api/setFBToken', {token: currentToken})
      .catch((err) => {
        console.log(err);
      });
    setTokenSentToServer(true);
  } else {
    $axios.post('/api/setFBToken', {token: currentToken})
      .catch((err) => {
        console.log(err);
      });
  }
}

export function initialiseFCM() {
  console.log('Requesting permission...');
  Notification.requestPermission().then((permission) => {
    if (permission === 'granted') {
      console.log('Notification permission granted.');
      resetUI();
    } else {
      console.log('Unable to get permission to notify.');
    }
  });
}

// resetUI();
