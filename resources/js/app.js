import 'babel-polyfill'
import Vue from 'vue'

import router from '~/router/index'
import store from '~/store/index'
import App from '$comp/App'
import '~/plugins/index'
import vuetify from '~/plugins/vuetify'
import axios from 'axios'
import sweetalert2 from 'sweetalert2'

window.$router = router;
window.$axios = axios;
Vue.prototype.$swal = sweetalert2;

export const app = new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
