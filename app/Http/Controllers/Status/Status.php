<?php

namespace App\Http\Controllers\Status;

use App\Http\Controllers\Sites\Site;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends Model
{
    use SoftDeletes;
    protected $table = 'statuses';
//    protected $fillable = ['active', 'host', 'interval'];

    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}
