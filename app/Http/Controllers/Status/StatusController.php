<?php

namespace App\Http\Controllers\Status;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatusController extends Controller
{
    public function index(Request $request){

    }

    public function show(Request $request, int $id_site)
    {
        $statuses = DB::table('statuses')
            ->where('id_site', $id_site)
            ->get()
            ->toArray();

        return json_encode($statuses);
    }

    public function store(Request $request)
    {

    }

    public function update(Request $request, Status $site){

    }

}
