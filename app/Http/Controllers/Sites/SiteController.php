<?php

namespace App\Http\Controllers\Sites;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class SiteController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();
        $sites = $user->sites;

        foreach ($sites as &$site) {
            $statuses = $site->statuses;
            $count = $statuses->count();
            $uptime = $statuses->where('status', 1)->count();
            $site->uptime = round(($uptime * 100) / $count, 2);
            $site->latest_status = $statuses->last()->status;
        }


        if ($sites) {
            $sites = $sites->toArray();
        }

        return json_encode($sites);
    }

    public function show(Request $request, int $id)
    {
        $user = $request->user();
        $site = Site::find($id);
        if ($site->user_id != $user->id) {
            throw new \Exception('Not allowed');
        }

        if ($site) {
            $sites = $site->toArray();
        }
        return json_encode($site);
    }

    public function destroy(Request $request, int $id)
    {
        $user = $request->user();
        $site = Site::find($id);
        if ($site->user_id != $user->id) {
            throw new \Exception('Not allowed');
        }

        $res = $site->delete();
        return json_encode($res);


    }

    public function store(Request $request)
    {
        $request->validate([
            'url' => 'required',
            'interval' => 'required',
        ]);

        $data = $request->all();
        $user = $request->user();

        $exists = \DB::table('sites')
            ->where([
                ['host', '=', $data['url']],
                ['user_id', '=', $user->id]
            ])
            ->exists();;
        if ($exists) {
            throw new \Exception('Site already exists');
        }

        $site = new Site();
        $site->host = $data['url'];
        $site->interval = $data['interval'];
        $site->user_id = $user->id;
        $site->last_run_on = null;
        $site->active = true;
        $res = $site->save();

        if ($res) {
            return json_encode($site);
        } else {
            throw new \Exception('Error adding the site');
        }
    }

    public function update(Request $request, Site $site)
    {
        $user = $request->user();
        if ($site->user_id != $user->id) {
            throw new \Exception('Not allowed');
        }

        $to_update = $request->all();
        $site->active = $to_update['active'];
        $site->interval = $to_update['interval'];
        $site->push_notification = $to_update['push_notification'];
        $res = $site->save();

        if ($res) {
            return json_encode($site);
        } else {
            throw new \Exception('Error updating data');
        }

    }

    public function checkSite(Request $request, int $id)
    {
        $exitCode = Artisan::call('state:check', [
            '--id' => $id
        ]);
        $exit_code = Artisan::output();

        return json_encode($exitCode);
    }
}
