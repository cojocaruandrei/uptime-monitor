<?php

namespace App\Http\Controllers\Sites;

use App\Http\Controllers\Status\Status;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Site extends Model
{
    use SoftDeletes;
    protected $table = 'sites';
    protected $fillable = ['active', 'host', 'interval'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function statuses()
    {
        return $this->hasMany(Status::class, 'id_site');
    }
}
