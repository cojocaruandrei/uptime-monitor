<?php

namespace App\Console\Commands;

use App\Http\Controllers\Sites\Site;
use App\Http\Controllers\Status\Status;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class StateCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'state:check {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check sanity of a host';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    function pushNotification($device_token, $host, $time){
        $params = json_encode(array(
            'notification' => array(
                'title' => $host . ' is down currently!',
                'body' => 'Checked at ' . $time,
                'icon' => '',
                'click_action' => 'https://andreic.develop.eiddew.com/',
                'color' => 'red'
            ),
            'to' => $device_token,
        ));

        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        $headers = array();
        $headers[] = 'Authorization: key=AAAAqIJ1nXE:APA91bGvPx42rgosWQl7AlohjQWoQxEj8_AFj6NL-P2TvjCfFaaRRN9ngXcnHV09JY3Y0d0GymJPTRl_imPCUv5dEB2m27YrRDpJ0cXUzGohZh-rEpG4-nUIuzQk_3bwQQPOIy1BNf7M';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Postman-Token: 578937c8-8718-4188-a098-fa0f6ed2bda2';
        $headers[] = 'Cache-Control: no-cache';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
    }

    function url_test( $url ) {
        $timeout = 10;
        $ch = curl_init();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $ch, CURLOPT_TIMEOUT, $timeout );
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $http_respond = curl_exec($ch);
        $http_respond = trim( strip_tags( $http_respond ) );
        $http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        curl_close( $ch );
        if ( ( $http_code == "200" ) || ( $http_code == "302" ) ) {
            return $http_code;
        } else {
            // return $http_code;, possible too
            return false;
        }
    }

    function newStatus(Site $site, $ignore_time = false){
        $current = Carbon::now();

        if(!empty($site->last_run_on) && !$ignore_time){
            $last_check = $site->last_run_on;
            $diff_in_mins = Carbon::parse($last_check)->diffInMinutes($current);

            if($diff_in_mins < $site->interval || $site->in_progress){
                return true;
            }

        }
        $site->in_progress = 1;
        $site->save();

        $status_code = self::url_test( $site->host );
        if(!$status_code && $site->push_notification){
            $device_tokens = User::find($site->user_id)->firebaseTokens;
            foreach ($device_tokens as $token){
                self::pushNotification($token->token, $site->host, $current);
            }
        }

        $status = new Status();
        $status->id_site = $site->id;
        $status->status = $status_code ? true : false;
        $status->status_code = $status_code;
        $res = $status->save();

        $site->last_run_on = $status->created_at;
        $site->in_progress = 0;
        $res2 = $site->save();

        if($res && $res2){
            return true;
        }
        return false;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $individual = (int)$this->option('id');
        if(!$individual) {
            $sites = Site::all();
            foreach ($sites as $site) {
                if ($site->active) {
                    self::newStatus($site);
//                    if (!$res) {
//                        dd(false);
//                    }
                }
            }
        }else{
            $site = Site::find($individual);
            self::newStatus($site, true);
        }

        dd(true);
    }
}
